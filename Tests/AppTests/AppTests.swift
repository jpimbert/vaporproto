@testable import App
import Dispatch
import XCTest
import Vapor

final class AppTests : XCTestCase {
    var app: Application!

    override func setUp() {
        do {
            var config = Config.default()
            var env = try Environment.detect()
            var services = Services.default()
            // this line clears the command-line arguments
            env.commandInput.arguments = []
            try App.configure(&config, &env, &services)
            app = try Application(
                config: config,
                environment: env,
                services: services
            )
            try App.boot(app)
            try app.asyncRun().wait()
            //  Reset database at starting
            _ = try? app.client().get("http://localhost:8080/reset").wait()
        } catch {
            fatalError("Failed to launch Vapor server: \(error.localizedDescription)")
        }
    }
    
    override func tearDown() {
        try? app.runningServer?.close().wait()
    }
    
    func createFile() -> UserFile {
        let fm = FileManager()
        let data = fm.contents(atPath: "/Groups/JPI-Conseil/Administration/À traiter/Factures/Factures fournisseur à traiter complètement/AA76398250.pdf")!
        let file = File(data: data, filename: "AA76398250.pdf")
        return UserFile(uploadfile: file, filetype: "Protocole", filetitle: "Facture n°1", filecomment: "Commentaire de la facture numéro 1")
    }
    
    func testNoFiles() throws {
        let response = try app.client().get("http://localhost:8080/backend").wait()
        let content = try response.content.syncDecode(ViewContent.self)
        XCTAssertEqual(content.count, 0, "There should be no file")
        XCTAssertEqual(content.files.count, 0, "There should be no file")
    }
    
    func testDeleteNonexistentFile() throws {
        let response = try app.client().get("http://localhost:8080/backend/delete/-1").wait()
        let content = try response.content.syncDecode(ViewContent.self)
        XCTAssertNotNil(content.error)
    }
    
    func testCreateFile() throws {
        _ = try app.client().post("http://localhost:8080/upload") { postRequest in
            try postRequest.content.encode(createFile())
            }.wait()
        let response = try app.client().get("http://localhost:8080/backend").wait()
        let content = try response.content.syncDecode(ViewContent.self)
        XCTAssertEqual(content.count, 1, "There should be one file")
        XCTAssertEqual(content.files.count, 1, "There should be no file")
}
    
    func testCreateBadFile() throws {
        let content = ["content": "This isn't really a post"]
        _ = try app.client().post("http://localhost:8080/upload") { postRequest in
            try postRequest.content.encode(content)
            }.wait()
        let response = try app.client().get("http://localhost:8080/backend").wait()
        let contentResponse = try response.content.syncDecode(ViewContent.self)
        XCTAssertEqual(contentResponse.count, 0, "There should be one file")
        XCTAssertEqual(contentResponse.files.count, 0, "There should be no file")
}

    static let allTests = [("testNoFiles", testNoFiles), ("testDeleteNonexistentFile",testDeleteNonexistentFile), ("testCreateFile",testCreateFile), ("testCreateBadFile",testCreateBadFile),]
    
}
