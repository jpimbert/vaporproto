import Routing
import Vapor
import Foundation

struct ViewContent:Content {
    var count:Int
    var files:[SKWFile]
    var error:String?
}

/// Register your application's routes here.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#routesswift)
public func routes(_ router: Router) throws {
    let hc = FileUploadController()
    
    //  Main page
    router.get { req -> Future<View> in
        return SKWFile.query(on: req).all().flatMap(to: View.self)
        { files in
            let content=ViewContent(count: files.count, files: files, error:nil)
            return try req.view().render("home", content)
        }
    }
    
    router.get("backend") { req -> Future<ViewContent> in
        return SKWFile.query(on: req).all().map(to: ViewContent.self)
        { files in
            return ViewContent(count: files.count, files: files, error:nil)
        }
    }
    
    //  Delete a file
    router.get ("delete", Int.parameter) { req -> Future<Response> in
        let fileID = try req.parameters.next(Int.self)
        return SKWFile.find(fileID, on:req).map(to: Response.self) { file -> Response in
            file?.deleteFile()
            _ = file?.delete(on: req)
            return req.redirect(to: "/")
        }
    }
    
    router.get("backend", "delete", Int.parameter) { req -> Future<ViewContent> in
        let fileID = try req.parameters.next(Int.self)
        return SKWFile.find(fileID, on:req).map(to: ViewContent.self) { file -> ViewContent in
            if ((file) != nil) {
                file?.deleteFile()
                _ = file?.delete(on: req)
                return ViewContent(count: 0, files: [], error:nil)
            } else {
                return ViewContent(count: 0, files: [], error: "Error while deleting \(fileID)")
            }
        }
    }

    //  Add a File
    router.get ("addfile") { req -> Future<View> in
        return try req.view().render("addfile")
    }
    
    //  Present the form to upload a file
    router.post("upload", use: hc.uploadFile)
    
    //  Reset the whole Database and Files
    router.get ("reset") { req -> Future<Response> in
        return SKWFile.query(on: req).all().map(to: Response.self) { files in
            for file in files {
                file.deleteFile()
                _ = file.delete(on: req)
            }
            return req.redirect(to: "/")
        }
    }
}
