//
//  FileUploadController.swift
//  App
//
//  Created by Jean-Pierre IMBERT on 21/07/2019.
//

import Foundation
import Routing
import Vapor

final class FileUploadController {
    func uploadFile(_ req: Request) throws -> Future<Response> {
        
        // pull out the multi-part encoded form data
        return try req.content.decode(UserFile.self).map(to: Response.self) { data in
            
            //  Try to upload File
            try data.upload()
            
            //  Create SKWFile attached to just uploaded file
            let skwFile = SKWFile(content:data)

            _ = skwFile.save(on: req)
            
            return req.redirect(to: "/")
        }
    }
}
