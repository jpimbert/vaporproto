//
//  Message.swift
//  App
//
//  Created by SkwalSoft on 20/07/2019.
//

import Foundation
import Fluent
import FluentSQLite
import Vapor

struct UserFile: Content {
    var uploadfile: File
    var filetype: String
    var filetitle: String
    var filecomment: String
    
    func upload() throws {
        // ensure this image is one of the valid types
        guard let mimeType = uploadfile.contentType else {
            // the message doesn't exist - bail out!
            throw Abort(.notFound)
        }
        guard UserFile.acceptableTypes().contains(mimeType) else {
            // the message doesn't exist - bail out!
            throw Abort(.notFound)
        }
        // replace any spaces in filenames with a dash
        let cleanedFilename = uploadfile.filename.replacingOccurrences(of: " ", with: "-")
        
        // convert that into a URL we can write to
        let newURL = UserFile.uploadDirectory().appendingPathComponent(cleanedFilename)
        
        // write the full-size original image
        _ = try? uploadfile.data.write(to: newURL)
    }
    
    private static func acceptableTypes() -> [MediaType] {
        return [MediaType.pdf]
    }
    
    private static func rootDirectory() -> String {
        return DirectoryConfig.detect().workDir
    }
    
    static func uploadDirectory() -> URL {
        return URL(fileURLWithPath: "\(self.rootDirectory())Public/LocalFiles")
    }
}

struct SKWFile: Content, SQLiteModel, Migration {
    var id: Int?
    var title: String
    var comment: String
    var type: String
    var name: String
    var date: Date
    
    init(content:UserFile) {
        self.title = content.filetitle
        self.comment = content.filecomment
        self.type = content.filetype
        self.name = content.uploadfile.filename
        self.date = Date()
    }
    
    func deleteFile() {
        let fileURL = UserFile.uploadDirectory().appendingPathComponent(name)
        let fm = FileManager()
        
        do {
            try fm.removeItem(at: fileURL)
        } catch {}
    }
}




