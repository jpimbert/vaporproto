import Vapor
import Leaf
import FluentSQLite

/// Called before your application initializes.
///
/// [Learn More →](https://docs.vapor.codes/3.0/getting-started/structure/#configureswift)
public func configure(
    _ config: inout Config,
    _ env: inout Environment,
    _ services: inout Services
) throws {
    // Register routes to the router
    let router = EngineRouter.default()
    try routes(router)
    services.register(router, as: Router.self)

    // Configure the Leaf Renderer
    try services.register(LeafProvider())
    config.prefer(LeafRenderer.self, for: ViewRenderer.self)
    
    //  Configure the File manager
    var middleware = MiddlewareConfig.default()
    middleware.use(FileMiddleware.self)
    services.register(middleware)
    services.register(NIOServerConfig.default(maxBodySize: 20_000_000))     // Increase max file size to upload

    //  Configure database SQLite
    let directoryConfig = DirectoryConfig.detect()
    services.register(directoryConfig)
    try services.register(FluentSQLiteProvider())
    var databaseConfig = DatabasesConfig()
    let db = try SQLiteDatabase(storage: .file(path: "\(directoryConfig.workDir)protocols.db"))
        databaseConfig.add(database: db, as: .sqlite)
        services.register(databaseConfig)
    var migrationConfig = MigrationConfig()
    migrationConfig.add(model: SKWFile.self, database: .sqlite)
    services.register(migrationConfig)
}
